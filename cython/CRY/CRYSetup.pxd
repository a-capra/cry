
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.map cimport map
    
    
cdef extern from "CRYSetup.h" namespace "CRYSetup": 
    cpdef enum CRYParms:
        parmMin,
        returnNeutrons=parmMin,  #include neutrons in return list (0,1)
        returnProtons,           #include protons?
        returnGammas,            #include gammas?
        returnElectrons,         #include electrons?
        returnMuons,             #include muons?
        returnPions,             #include pions?
        returnKaons,             #include kaons?
        subboxLength,            #length of box to return particles in 
        altitude,                #Working altitude 
        latitude,                #Working latitude
        date,                    #Date (for solar cycle)
        nParticlesMin,           #Minimum # of particles to return
        nParticlesMax,           #Maximum # of particles to return
        xoffset,
        yoffset,
        zoffset,
        parmMax=zoffset     
    
    
cdef extern from "CRYSetup.h":
    cdef cppclass CRYSetup:       
        
        CRYSetup(string configData, string dataDir)
    
        double param(CRYParms parm)
        void setParam(CRYParms parm, double value)

        

cdef class Setup:
    cdef CRYSetup *thisptr

