#!python
#cython: embedsignature = True
#cython: language_level = 3
#distutils: language = c++

from .CRYSetup cimport Setup
from .CRYParticle cimport Particle

cdef class Generator:

    def __cinit__(self, Setup setup):
        self.thisptr = new CRYGenerator(setup.thisptr)
        
    def genEvent(self):
        cdef vector[CRYParticle*] result
        self.thisptr.genEvent(&result)
        cdef int size = result.size()
        print("Ran and got", size, "hits")
        output = []
        for i in range(size):
            output.append(Particle.create_and_own(result[0]))
        
        return output
        
        
    def timeSimulated(self):
        return self.thisptr.timeSimulated()
        
    def primaryParticle(self):
        return Particle.create(self.thisptr.primaryParticle())

    def boxSizeUsed(self):
        return self.thisptr.boxSizeUsed()