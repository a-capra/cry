from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.map cimport map
    
    
cdef extern from "CRYParticle.h" namespace "CRYParticle": 
    cpdef enum CRYId:
         CRYIdMin,
         Neutron=CRYIdMin,
         Proton,
         Pion,
         Kaon,
         Muon,
         Electron,
         Gamma,
         CRYIdMax=Gamma
        
            
cdef extern from "CRYParticle.h":
    cdef cppclass CRYParticle:       
        CRYParticle( CRYId id, int charge, double KE)
        CRYParticle( const CRYParticle &part)
        
        void setPosition(double x, double y, double z)
        void setDirection(double u, double v, double w)
        
        void setTime(double t)

        double ke()
          
        double x()
        double y()
        double z()
        
        double u()
        double v()
        double w()
        int charge()
        
        double t()
        
        CRYId id()
        
        int PDGid()
        
        void fill(CRYId &id, int &q,
          double &ke, double &x, double &y, double &z,
          double &u, double &v, double &w, double &t)

        

cdef class Particle:
    cdef CRYParticle *thisptr
    cdef bint ownership
    

    @staticmethod
    cdef create(CRYParticle* ptr)
    
    @staticmethod
    cdef create_and_own(CRYParticle* ptr)